/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.pti.myatm;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * @author SM
 */
public class ATMTest {

/*
 *    initial test
    @Test
    public void testGetMoneyInATM() {
        System.out.println("getMoneyInATM");
        ATM instance = null;
        double expResult = 0.00;
        double result = instance.getMoneyInATM();
        assertEquals(expResult, result, 0.0);
        fail("The test case is a prototype.");
    }
*/

    //  getMoneyInATM - Test #1
    @Test
    public void testGetMoneyInATMOk() {
        System.out.println("getMoneyInATM - Test #1 ");
        ATM atm = new ATM(5000);
        double expResult = 5000;
        double result = atm.getMoneyInATM();
        assertEquals(expResult, result, 0.0);
        System.out.println(" Expected " + expResult + " = " + " Actual " + result);
    }


    //  getMoneyInATM - Test #2
    @Test
    public void testGetMoneyInATMFail() {
        System.out.println("getMoneyInATM - Test #2 ");
        ATM atm = new ATM(5000.20);
        double expResult = 5000;
        double result = atm.getMoneyInATM();
        assertEquals(expResult, result, 0.0);
        System.out.println(expResult + " = " + result);
    }


    //  getMoneyInATM - Test #3
    @Test
    public void testGetMoneyInATMFailed() {
        System.out.println("getMoneyInATM - Test #3 ");
        ATM atm = new ATM(0);
        double expResult = 5000;
        double result = atm.getMoneyInATM();
        assertEquals(expResult, result, 0.0);
        System.out.println(expResult + " = " + result);
    }

/*
 *    initial test
    @Test
    public void testValidateCard() {
        System.out.println("validateCard");
        Card card = null;
        int pinCode = 0;
        ATM instance = null;
        boolean expResult = false;
        boolean result = instance.validateCard(card, pinCode);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

*/

    @Test
    public void testValidateCardOk() throws NoCardInserted {
        System.out.println("validateCard - Test #4");
        ATM atm = new ATM(500);
        int pinCode = 1111;

        Card card = mock(Card.class);
        when(card.isBlocked()).thenReturn(false);
        when(card.checkPin(pinCode)).thenReturn(true);

        assertTrue(atm.validateCard(card, pinCode));
    }

    @Test
    public void testValidateCardFail() throws NoCardInserted {
        System.out.println("validateCard - Test #5");

        ATM atm = new ATM(0);
        int pinCode = 0;
        Card card = null;
        boolean expResult = false;
        boolean result = atm.validateCard(card, pinCode);
        assertEquals(expResult, result);
        assertTrue(atm.validateCard(card, pinCode));
    }

    @Test(expected = NullPointerException.class)
    public void testValidateCardNullCard() throws NoCardInserted {
        System.out.println("validateCard - Test #6");
        ATM atm = new ATM(5000);
        int pinCode = 1111;
        atm.validateCard(null, pinCode);
    }


/*
 *    initial test
    @Test
    public void testCheckBalance() {
        System.out.println("checkBalance");
        ATM instance = null;
        double expResult = 0.0;
        double result = instance.checkBalance();
        assertEquals(expResult, result, 0.0);
        fail("The test case is a prototype.");
    }
*/

    @Test
    public void testCheckBalanceOk() throws NoCardInserted {
        System.out.println("checkBalance - Test #7");

        ATM atm = new ATM(5000);
        double moneyInATM = atm.getMoneyInATM();
        double expResult = 5000;

        int pinCode = 1111;

        Account account = mock(Account.class);
        when(account.getBalance()).thenReturn(moneyInATM);

        Card card = mock(Card.class);
        when(card.getAccount()).thenReturn(account);
        when(card.isBlocked()).thenReturn(false);
        when(card.checkPin(pinCode)).thenReturn(true);

        atm.validateCard(card, pinCode);
        atm.checkBalance();

        double result = atm.checkBalance();

        assertEquals(expResult, result, 0.0);

        System.out.println(expResult + " = " + result);
    }


    @Test
    public void testCheckBalanceFail() throws NoCardInserted {
        System.out.println("checkBalance - Test #8");

        ATM atm = new ATM(5000);
        double moneyInATM = atm.getMoneyInATM();
        double expResult = 7000;

        int pinCode = 1111;

        Account account = mock(Account.class);
        when(account.getBalance()).thenReturn(moneyInATM);

        Card card = mock(Card.class);
        when(card.getAccount()).thenReturn(account);
        when(card.isBlocked()).thenReturn(false);
        when(card.checkPin(pinCode)).thenReturn(true);

        atm.validateCard(card, pinCode);
        atm.checkBalance();

        double result = atm.checkBalance();

        assertEquals(expResult, result, 0.0);

        System.out.println(expResult + " = " + result);
    }




/*
 *    initial test

    @Test
    public void testGetCash() throws NotEnoughMoneyInATM, NotEnoughMoneyInAccount {
        System.out.println("getCash");
        double amount = 0.0;
        ATM instance = null;
        double expResult = 0.0;
        double result = instance.getCash(amount);
        assertEquals(expResult, result, 0.0);
        fail("The test case is a prototype.");
    }
*/

    @Test
    public void testGetCashOk() throws NoCardInserted, NotEnoughMoneyInATM, NotEnoughMoneyInAccount, NoCardInsertedException {
        System.out.println("getCash - Test #9");

        double amount = 1000;

        ATM atm = new ATM(5000);
        int pinCode = 1111;
        double actualValue = 5000;


        Account account = mock(Account.class);
        when(account.getBalance()).thenReturn(actualValue);

        Card card = mock(Card.class);
        when(card.checkPin(pinCode)).thenReturn(true);
        when(card.getAccount()).thenReturn(account);
        when(card.isBlocked()).thenReturn(false);

        atm.validateCard(card, pinCode);
        atm.getCash(amount);

    }

    @Test
    public void testGetCashOkay() throws NoCardInserted, NotEnoughMoneyInATM, NotEnoughMoneyInAccount, NoCardInsertedException {
        System.out.println("getCash - Test #10");

        double amount = 5000;

        ATM atm = new ATM(5000);
        int pinCode = 1111;

        Account account = mock(Account.class);
        double value = 5000;
        double expResult = 5000;

        Card card = mock(Card.class);
        when(account.getBalance()).thenReturn(value);
        when(card.getAccount()).thenReturn(account);
        when(card.isBlocked()).thenReturn(false);
        when(card.checkPin(pinCode)).thenReturn(true);

        atm.validateCard(card, pinCode);
        double result = atm.getCash(amount);

        assertEquals(expResult, result, 0.0);

        System.out.println(expResult + " = " + result);
    }

    @Test
    public void testGetCashFailed() throws NoCardInserted, NotEnoughMoneyInATM, NotEnoughMoneyInAccount, NoCardInsertedException {
        System.out.println("getCash - Test #11");

        double amount = 15000;

        ATM atm = new ATM(5000);
        int pinCode = 1234;

        Account account = mock(Account.class);
        double value = 5000;
        double expResult = 5000;

        Card card = mock(Card.class);
        when(account.getBalance()).thenReturn(value);
        when(card.getAccount()).thenReturn(account);
        when(card.isBlocked()).thenReturn(false);
        when(card.checkPin(pinCode)).thenReturn(true);

        atm.validateCard(card, pinCode);
        double result = atm.getCash(amount);

        assertEquals(expResult, result, 0.0);

        System.out.println(expResult + " = " + result);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testSetNegativeMoneyInATMThrownIllegalArgumentExceptionOk() {

        System.out.println("Negative money in ATM - Test #12");
        new ATM(-5000);
    }

    @Test
    public void testSetNegativeMoneyInATMThrownIllegalArgumentExceptionFailed() {

        System.out.println("Negative money in ATM - Test #13");
        new ATM(-5000);
    }

    @Test(expected = NoCardInsertedException.class)
    public void testGetCashCardIsNullThrownNoCardInsertedOk() throws NoCardInsertedException, NotEnoughMoneyInATM, NotEnoughMoneyInAccount {

        System.out.println("No card inserted in ATM - Test #14");
        ATM atm = new ATM(5000);
        double amount = 1111;
        assertNull(atm.getCash(amount));
    }


    @Test
    public void testGetCashCardIsNullThrownNoCardInsertedFail() throws NoCardInsertedException, NotEnoughMoneyInATM, NotEnoughMoneyInAccount {

        System.out.println("No card inserted in ATM - Test #15");
        ATM atm = new ATM(5000);
        double amount = 1111;
        assertNull(atm.getCash(amount));
    }


}
